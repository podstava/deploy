#!/bin/sh

# Install cmake, ssl lib, git, pygit2

apt-get update
apt-get -y install cmake libssl-dev git python-pip python-dev libffi-dev libjpeg-dev python-mysqldb libmysqlclient-dev
wget https://github.com/libgit2/libgit2/archive/v0.25.0.tar.gz
tar xfz v0.25.0.tar.gz
cd libgit2-0.25.0/
cmake .
make
sudo make install
ldconfig
pip install pygit2==0.25.0

# Install salt 

wget -O - https://repo.saltstack.com/apt/ubuntu/14.04/amd64/archive/2016.11.3/SALTSTACK-GPG-KEY.pub | apt-key add - ;
echo 'deb http://repo.saltstack.com/apt/ubuntu/14.04/amd64/archive/2016.11.3 trusty main' >> /etc/apt/sources.list.d/saltstack.list;
apt-get update;
apt-get -y install salt-minion;

# Configure salt

echo '
file_client: local

fileserver_backend:
  - git
  - roots

gitfs_provider: pygit2

gitfs_remotes:
  - https://github.com/sibtc/django-beginners-guide.git:
    - mountpoint: salt://git-backend/stub
    # - user: 
    # - password:  
    - base: master
' > /etc/salt/minion;

cd /srv
git clone https://podstava@bitbucket.org/podstava/saltypopcorn.git
cp -r saltypopcorn/salt/ ./ ; cp -r saltypopcorn/pillar/ ./ ; rm -rf saltypopcorn
echo '
dbname: test_db
dbuser: test_user
dbpass: test_pass
dbhost: localhost
dbport: 3306
fs_path: /var/www
app_name: myproject
app_settings: myproject.settings
app_fqdn: test.fqdn
locale:
  - name: en_US.utf8
    system: True
user:
  username: testuser
  pkey: AAAAB3NzaC1yc2EAAAADAQABAAABAQClZDHRiNX8ix8gxKvKfwYDCVBrvU+b5aPQdsUe3R0Dbus2+XRESXMNqi+FUFFpBVk92wb3CP8Vg2o5L51vrbYxu5q4/u1456LDYxQfzfEnrK5IVwnE0DB+CubqHksjuu32b4BIDRfOh72+aZ9ba6fQbfrUoHa9X4GRYyN2IWBd6t8eD2Pbkh9TORh68tYBOem7/7SBd7kxKH5N4H7tnT/1mKQfsZ/g6N22IJ6GXoTyHPzc/dSKWsAv2mRcnamvi1NitQukeLYgtDyTRnG49/EvbvpEsmTMy+OfBspqTpcvn8cOu8SoMMaRW1yA2VMUGJYy4tZB08YIsa9yrbtdp8J1
root_user: crocodile

' >> pillar/config.sls

salt-call --local state.apply tor; salt-call --local state.apply
