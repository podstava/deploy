from paramiko import AutoAddPolicy
from paramiko.client import SSHClient
from requests.auth import HTTPBasicAuth

ip = '14.88.14.88'
username = 'root'
pkey = 'public key'

client = SSHClient()
client.set_missing_host_key_policy(AutoAddPolicy())
client.connect(hostname=ip,
               username=username,
               pkey=pkey)

stdin, stdout, stderr = client.exec_command('sudo salt-call --local state.apply')
stdout.read()  # Wait for response

client.close()

