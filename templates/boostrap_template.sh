#!/bin/sh

if [! -d libgit2-0.25.0 ]; then
# Install cmake, ssl lib, git, pygit2
apt-get update
apt-get -y install cmake libssl-dev git python-pip python-dev libffi-dev libjpeg-dev python-mysqldb libmysqlclient-dev
wget https://github.com/libgit2/libgit2/archive/v0.25.0.tar.gz
tar xfz v0.25.0.tar.gz
cd libgit2-0.25.0/
cmake .
make
sudo make install
ldconfig
pip install pygit2==0.25.0
fi

# Install salt 

wget -O - https://repo.saltstack.com/apt/ubuntu/14.04/amd64/archive/2016.11.3/SALTSTACK-GPG-KEY.pub | apt-key add - ;
echo 'deb http://repo.saltstack.com/apt/ubuntu/14.04/amd64/archive/2016.11.3 trusty main' >> /etc/apt/sources.list.d/saltstack.list;
apt-get update;
apt-get -y install salt-minion;

# Configure salt

echo '
file_client: local

fileserver_backend:
  - git
  - roots

gitfs_provider: pygit2

gitfs_remotes:
  - {{ git_project }}:
    - mountpoint: salt://git-backend/stub
    # - user: {{ git_user }}
    # - password: {{ git_pass }} 
    - base: master
' > /etc/salt/minion;

cd /srv
git clone https://podstava@bitbucket.org/podstava/saltypopcorn.git
cp -r saltypopcorn/salt/ ./ ; cp -r saltypopcorn/pillar/ ./ ; rm -rf saltypopcorn
echo '
dbname: {{ dbname }}
dbuser: {{ dbuser }}
dbpass: {{ dbpass }}
dbhost: localhost
dbport: 3306
fs_path: {{ fs_path }}
app_name: {{ app_name }}
app_settings: {{ settings }}
app_fqdn: test.fqdn
locale:
  - name: en_US.utf8
    system: True
user:
  username: {{ sys_user }}
  pkey: {{ public_key }}
root_user: {{ root_user }}

' >> pillar/config.sls

salt-call --local state.apply tor; salt-call --local state.apply

