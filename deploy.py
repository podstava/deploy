#!/usr/bin/env python

import os
import io

from jinja2 import Environment, FileSystemLoader

from paramiko import AutoAddPolicy
from paramiko.client import SSHClient
from paramiko import RSAKey

from requests.auth import HTTPBasicAuth
from scp import SCPClient

PATH = os.path.dirname(os.path.abspath(__file__))
TEMPLATE_ENVIRONMENT = Environment(
        autoescape=False,
        loader=FileSystemLoader(os.path.join(PATH, 'templates')),
        trim_blocks=False)
ip = '23.101.65.121'
username = 'crocodile'
path_to_pkey = '/home/podstava/.ssh/id_rsa'
if not path_to_pkey:
    print('Need path to private key')
    quit()

pkey = RSAKey.from_private_key_file(path_to_pkey)


def render_template(template_filename, context):
    print('Rendering template')
    return TEMPLATE_ENVIRONMENT.get_template(template_filename).render(context)


def patch_sh_script():
    # TODO: add more configurable vars
    git_project = 'https://github.com/sibtc/django-beginners-guide.git'
    app_name = 'myproject'
    file_name = 'bootstrap_git_salt.sh'
    context = {
        'git_project': git_project,
        'app_name': app_name,
        'dbname': 'test_db',
        'dbuser': 'test_user',
        'dbpass': 'test_pass',
        'fs_path': '/var/www',
        'public_key': pkey.get_base64(),
        'sys_user': 'testuser',
        'root_user': username,
        'settings': 'myproject.settings'
    }
    with open(file_name, 'w') as f:
        ren_script = render_template('boostrap_template.sh', context)
        f.write(ren_script)
patch_sh_script()
print('Connecting to server')
client = SSHClient()
client.set_missing_host_key_policy(AutoAddPolicy())
client.connect(hostname=ip,
               username=username,
               pkey=pkey)

with SCPClient(client.get_transport()) as scp:
    scp.put('bootstrap_git_salt.sh')
print('Deploying. This process can take 5-10 minutes.')
stdin, stdout, stderr = client.exec_command('sudo sh bootstrap_git_salt.sh')
stdout.read()  # Wait for deploy

client.close()

